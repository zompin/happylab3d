<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="top-menu">
		<a href="https://happylab.ru/" class="logo top-menu__logo"></a>
		<div class="top-menu__right">
			<nav class="top-menu__nav">
				<a href="#works" class="top-menu__nav-item">Наши работы</a>
				<a href="#services" class="top-menu__nav-item">Услуги</a>
				<a href="#whywe" class="top-menu__nav-item">Почему мы</a>
				<a href="#contacts" class="top-menu__nav-item">Контакты</a>
			</nav>
			<button class="button button_transparent calc top-menu__calc">Расчитать визуализацию</button>
			<div class="contacts top-menu__contacts">
				<a href="tel:84999513032" class="contacts__contact contacts__contact_phone top-menu__contacts-phone">8 499 951-30-32</a>
				<a href="mailto:info@happylab.ru" class="contacts__contact contacts__contact_email">info@happylab.ru</a>
			</div>
		</div>
		<button class="top-menu__button">
			<span></span>
		</button>
	</div>
	<div class="expand-menu">
		<div class="expand-menu__inner">
			<nav class="expand-menu__nav">
				<a href="#works" class="expand-menu__nav-item">Наши работы</a>
				<a href="#services" class="expand-menu__nav-item">Услуги</a>
				<a href="#whywe" class="expand-menu__nav-item">Почему мы</a>
			</nav>
			<a href="#calc" class="expand-menu__button">РАССЧИТАТЬ ВИЗУАЛИЗАЦИЮ</a>
			<div class="contacts expand-menu__contacts">
				<a href="tel:84999513032" class="contacts__contact contacts__contact_phone expand-menu__contacts-phone">8 499 951-30-32</a>
				<a href="mailto:info@happylab.ru" class="contacts__contact contacts__contact_email expand-menu__contacts-email">info@happylab.ru</a>
			</div>
			<div class="expand-menu__address">127273, Москва, ул. Березовая аллея, д. 5А, стр. 5 <br> Пн-Пт: 10:00 – 19:00</div>
			<div class="expand-menu__copy">&copy; 2013-2017 HAPPYLAB</div>
		</div>
	</div>
	<header class="header">
		<div class="header__opacity"></div>
		<div class="header__inner">
			<div class="header__header h">Визуализируй это</div>
			<div class="header__text">Создаем качественные 3d-макеты архитектуры и интерьеров, которые решают ваши задачи</div>
			<button class="button button_yellow calc header__button">Рассчитать визуализацию</button>
		</div>
	</header>
	<a name="works"></a>
	<section class="made">
		<div class="made__item made__item_16x4 made__item_6x2 made__item_left made__item_head">
			<div class="made__item-inner made__item-inner_head">
				<div class="made__header h">МЫ ЭТО СДЕЛАЛИ</div>
				<div class="made__text">Каждый проект — это история, которую мы проживаем вместе с клиентом. От рождения идеи до ее реального воплощения, достигающего поставленных целей и задач.</div>
			</div>
		</div>
		<div class="made__item made__item_8x9 made__item_3x4 made__item_right">
			<img src="/content/8.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left">
			<img src="/content/18.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left">
			<img src="/content/4.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_16x5 made__item_6x2 made__item_left">
			<img src="/content/22.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_tablet-right made__item_mobile_hide">
			<img src="/content/6.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x10 made__item_3x4 made__item_left made__item_mobile_hide">
			<img src="/content/13.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_16x5 made__item_3x2 made__item_left made__item_tablet-right made__item_mobile_hide">
			<img src="/content/10.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/20.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/21.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_16x5 made__item_6x2 made__item_left made__item_mobile_hide">
			<img src="/content/5.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x10 made__item_3x4 made__item_right made__item_mobile_hide">
			<img src="/content/11.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/12.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/14.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_16x5 made__item_6x2 made__item_left made__item_mobile_hide">
			<img src="/content/23.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_right made__item_mobile_hide">
			<img src="/content/16.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/7.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/17.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x10 made__item_3x4 made__item_right made__item_mobile_hide">
			<img src="/content/1.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/9.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_6x2 made__item_left made__item_mobile_hide">
			<img src="/content/2.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/3.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_3x2 made__item_left made__item_mobile_hide">
			<img src="/content/15.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<div class="made__item made__item_8x5 made__item_6x2 made__item_left made__item_mobile_hide">
			<img src="/content/19.jpg">
			<div class="made__item-inner">
				<div class="made__item-cat"></div>
				<div class="made__item-title"></div>
			</div>
		</div>
		<button class="made__show">ПОКАЗАТЬ БОЛЬШЕ РАБОТ</button>
	</section>
	<a name="services"></a>
	<section class="offer">
		<div class="offer__header h">ЧТО МЫ ПРЕДЛАГАЕМ</div>
		<div class="offer__text">
			<div class="offer__text-inner">
				Съемка любой сложности «под ключ»: создание оригинальной идеи, подбор актеров, предоставление собственной видеостудии и профессионального оборудования. Полный цикл постпродакшна – монтаж, использование всех видов анимации, графики, озвучание. Какой бы ни была ваша задача — мы найдем максимально эффективное решение.
			</div>
		</div>
		<div class="offer__items">
			<div class="offer__item offer__item_interior">3D-ВИЗУАЛИЗАЦИЯ <br> ИНТЕРЬЕРА</div>
			<div class="offer__item offer__item_arch">3D-ВИЗУАЛИЗАЦИЯ <br> АРХИТЕКТУРЫ</div>
			<div class="offer__item offer__item_item">ПРЕДМЕТНАЯ <br> ВИЗУАЛИЗАЦИЯ</div>
			<div class="offer__item offer__item_calc calc">РАССЧИТАТЬ <br> ВИЗУАЛИЗАЦИЮ</div>
		</div>
	</section>
	<a name="calc"></a>
	<div class="order__cont">
		<div class="order__close-div"></div>
		<div class="order">
			<div class="order__inner">
				<button class="close order__close">&times;</button>
				<div class="order__header">Отправить заявку на расчет</div>
				<input type="text" class="input require input_name" placeholder="Ваше имя *">
				<input type="text" class="input require input_phone" placeholder="Телефон *">
				<input type="text" class="input require input_email" placeholder="e-mail *">
				<input type="text" class="input input_site" placeholder="Адрес сайта">
				<div class="order__subheader">Какая услуга вас интересует?</div>
				<label class="label">
					<input type="radio" name="service" value="3D-визуализация интерьеров" class="radio" checked>
					<span>3D-визуализация интерьеров</span>
				</label>
				<label class="label">
					<input type="radio" name="service" value="3D-визуализация архитектуры" class="radio">
					<span>3D-визуализация архитектуры</span>
				</label>
				<label class="label">
					<input type="radio" name="service" value="Предметная визуализация" class="radio">
					<span>Предметная визуализация</span>
				</label>
				<input type="text" class="input input_message" placeholder="Комментарий">
				<button class="button button_yellow order__button">
					<span>ОТПРАВИТЬ ЗАПРОС</span>
				</button>
				<div class="order__text">
					Еще  вы можете связаться с нами по электронной почте: <a href="mailto:info@happylab.ru" class="order__link">info@happylab.ru</a>
				</div>
			</div>
		</div>
	</div>
	<a name="whywe"></a>
	<section class="why">
		<div class="why__header h">ПОЧЕМУ МЫ?</div>
		<ul class="why__list ul">
			<li class="why__list-item li">Мы работаем только с опытными специалистами</li>
			<li class="why__list-item li">Строго соблюдаем сроки работ</li>
			<li class="why__list-item li">Оперативно реагируем на комментарии</li>
			<li class="why__list-item li">Считаем стоимость услуг по-честному</li>
			<li class="why__list-item li">Мы любим гордиться своей работой, поэтому подходим к каждому проекту, как к своему</li>
		</ul>
	</section>
	<footer class="footer">
		<div class="footer__inner">
			<a href="https://happylab.ru/" class="logo footer__logo"></a>
			<div class="footer__inner-right">
				<div class="contacts footer__contacts">
					<a href="tel:84999513032" class="contacts__contact contacts__contact_phone footer__contacts-phone">8 499 951-30-32</a>
					<a href="mailto:info@happylab.ru" class="contacts__contact contacts__contact_email footer__contacts-email">info@happylab.ru</a>
				</div>
				<div class="footer__address">127273, Москва, ул. Березовая аллея, д. 5А, стр. 5 <br> Пн-Пт: 10:00 – 19:00 </div>
			</div>
		</div>
	</footer>
	<button class="gallery__fixed gallery__close">&times;</button>
	<div class="gallery__fixed">
		<div class="gallery__cont">
			<div class="gallery">
				<div class="gallery__items"></div>
			</div>
		</div>
	</div>
	<div class="gallery__control">
		<button class="gallery__button gallery__fixed gallery__button_prev"></button>
		<button class="gallery__button gallery__fixed gallery__button_next"></button>
	</div>
	<script src="js/jquery.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>