;(function($) {
	var menu 	= $('.top-menu');
	var header 	= $('.header');

	var imgs = $('.made img').clone();
	$('.gallery__items').append(imgs);

	$(document)
		.on('click', '.calc', function() {
			if (!is768()) {
				bodyToggle()
			}

			$('.order__cont').show();
		})

		.on('click', '.order__close, .order__close-div', function() {
			if (!is768()) {
				bodyToggle()
			}

			$('.order__cont').hide();
		})

		.on('click', '.top-menu__button', function() {
			bodyToggle();
			$('.expand-menu').toggleClass('expand-menu_show');
			$('.top-menu__button').toggleClass('top-menu__button_close');
		})

		.on('click', '.expand-menu__nav-item, .expand-menu__button', function() {
			bodyToggle();
			$('.expand-menu').removeClass('expand-menu_show');
			$('.top-menu__button').removeClass('top-menu__button_close');

			if ($(this).attr('href').substr(1) == 'calc') {
				$('.order__cont').show();
			}
		})

		.on('click', '.order__button', function() {
			var checked = true;
			var button = $('.order__button');

			$('.order .input.require').each(function(i, e) {
				var el = $(e);

				if (el.val() == 0) {
					checked = false
					el.addClass('input_error');
				} else {
					el.removeClass('input_error');
				}
			});

			if (checked) {
				var name = $('.input_name').val();
				var phone = $('.input_phone').val();
				var email = $('.input_email').val();
				var site = $('.input_site').val();
				var service = $('.radio:checked').val();
				var comment = $('.input_message').val();
				var data = {
					name: name,
					phone: phone,
					email: email,
					site: site,
					service: service,
					comment: comment
				};

				button.prop('disabled', true);

				$.ajax({
					type: 'POST',
					url: '/order.php',
					data: data,
					success: function(data) {
						button.prop('disabled', false);

						if (data) {
							button.addClass('button_success');
							button.find('span').html('Ваш запрос отправлен!');
						} else {
							button.addClass('button_error');
						}
					},
					error: function() {
						button.prop('disabled', false);
						button.addClass('button_error');
					}
				});
			} else {
				alert("Вы заполнили не все обязательные поля");
			}

			setTimeout(function() {
				button.removeClass('button_error button_success');
				button.find('span').html('ОТПРАВИТЬ ЗАПРОС');
			}, 5000);
		})

		.on('change', '.order .input', function(e) {
			var el = $(e.target);
			var val = el.val();

			if (val.length > 0) {
				el.addClass('input_filled');
			} else {
				el.removeClass('input_filled');
			}
		})

		.on('click', '.made__show', function() {
			var projects = $('.made__item_mobile_hide');

			if (projects) {
				for (var i = 0; i < 4; i++) {
					if (projects[i]) {
						$(projects[i]).fadeIn().removeClass('made__item_mobile_hide');
					}
				}
			}

			projects = $('.made__item_mobile_hide');

			if (!projects.length) {
				$('.made__show').hide();
			}
		})

		.on('click', '.made__item:not(.made__item_head)', function() {
			
			$('.gallery__fixed').show();
			$('.gallery__items').slick({
				prevArrow: '.gallery__button_prev',
				nextArrow: '.gallery__button_next',
				fade: true,
				respondTo: 'min'
			});
			bodyToggle();
		})

		.on('click', '.gallery__close', function() {
			$('.gallery__fixed').hide();
			$('.gallery__items').slick('unslick');
			bodyToggle();
		})

		.on('click', '.header__button', function() {
			if (is768()) {
				console.log($('.order').offset().top)
				$('html, body').scrollTop($('.order').offset().top);
			}
		});

	$(document).scroll(function() {
		var offset 	= $(document).scrollTop();

		if (offset + parseInt(menu.css('height')) > header.height()) {
			menu.addClass('top-menu_solid');
		} else {
			menu.removeClass('top-menu_solid');
		}
	});

	function bodyToggle() {
		var body = $('body');

		if (body.hasClass('hidden')) {
			body.css('overflow', 'auto');
		} else {
			body.css('overflow', 'hidden');
		}

		body.toggleClass('hidden');
	}

	function is768() {
		return window.matchMedia('(max-width: 768px)').matches;
	}

})(jQuery);